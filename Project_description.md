Project
=======

You will create a program that is made up of many features that are placed in a number of files. The purpose of the program is to handle data on a large number of people in a list. The main program shall consist of a menu from which the user may...

• ... add a person to the list

• ... print the entire list of persons on the screen

• ... search for a person in the list

• ... delete a person from the list

• ... sort the list in different ways

• ... randomize the order of the list

• ... save the list to specified text file

• ... load list data from specified text file
