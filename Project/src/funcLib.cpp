//
// Created by pauli on 1/13/19.
//

#include "../include/funcLib.h"
#include "../include/constants.h"

using namespace std;

// = = = = = = = = = = = = = = = = = = = =
//      MENUES AND PRINTINGS
// = = = = = = = = = = = = = = = = = = = =
/*
* ===  FUNCTION  ======================================================================
*         Name:  createEntries()
*  Description:  Create a vector<string> with menu entries
*        Input:  Takes enum MenuTitle as input
*       Output:  A vector<string> with entries for given menu.
* =====================================================================================
*/
vector<string> createEntries(MenuTitle menu) {
    switch (menu) {
        case MainMenu:
            return vector<string> {
                    "1. Add a person",
                    "2. Print the list with persons",
                    "3. Sort or randomize the list",
                    "4. Search a person",
                    "5. Delete a person",
                    "6. Save the list to file",
                    "7. Load data from file",
                    "8. Find doublets in list",
                    "9. Exit"
            };
        case SortingMenu:
            return vector<string> {
                    "1. By last name",
                    "2. By signature",
                    "3. By height",
                    "4. Randomize",
            };
        default:
            cout << "Ups... I made a mistake, sorry!" << endl;
            break;
    }
}

/*
* ===  FUNCTION  ======================================================================
*         Name:  showMenu()
*  Description:  Prints Main Menu to screen. And ask user to select entry.
*        Input:  Takes MenuTitle (string) and menu entries (vector<std::string)
*                and an unsigned long to tell total count of persons in a given list.
*
*       Output:  Integer with users selection of menu entry.
* =====================================================================================
*/
int showMenu(const string menuTitle, const vector<string> entries, const unsigned long total) {
    cout << endl << endl << endl ;
    cout << menuTitle << endl;
    cout << "Total number of persons in the list: " << total << endl;
    for (auto e : entries) {
        cout << e << endl;
    }
    return getInteger("Please input your choice as an integer: ");
}

/*
* ===  FUNCTION  ======================================================================
*         Name:  showList()
*  Description:  Prints a list to screen
*        Input:  Takes a list (vector<std::string>)
*       Output:  -
* =====================================================================================
*/
void showList(vector<Person> data) {
    if (data.empty()) {
        cout << "Ups, I promised to much. The list is empty. You can add persons from the main menu." << endl;
    } else {
        // Initialization of constants. Determine size of the output table.
        constexpr unsigned ROWS{20};
        constexpr unsigned SMALLCOL{5};
        constexpr unsigned BIGCOL{35};
        constexpr unsigned SIGNATURECOL{15};

        unsigned i {1};
        for (auto &e : data) {
            // MAKES HEADER AND AND PAUSE WHEN 'ROWS' LINES ARE PRINTED ON SCREEN
            if ((i - 1) % ROWS == 0) {
                if (i > 1) {
                    waitForKey();
                }
                cout << setw(SMALLCOL + 1) << right << "NR." << setw(SIGNATURECOL) << left << " SIGNATURE"
                     << setw(BIGCOL + 1) << left << " NAME" << setw(SMALLCOL) << right << "HEIGHT" << endl;
            }
            cout << setw(SMALLCOL) << right
                 << i << ". "
                 << setw(SIGNATURECOL) << left
                 << e.signature
                 << setw(BIGCOL) << left
                 << (e.firstName + " " + e.lastName)
                 << setw(SMALLCOL) << right << setprecision(2) << fixed
                 << e.height
                 << endl;
            i++;
        }
    }
}

// = = = = = = = = = = = = = = = = = = = =
//      SEARCHING AND DELETING
// = = = = = = = = = = = = = = = = = = = =
/*
* ===  FUNCTION  ======================================================================
*         Name:  signatureSearch()
*  Description:  Searching with signature as search key.
*        Input:  Takes signature as search key (string).
 *               And a vector<Persons> as the list to search in.
*       Output:  vector<Persons> with all persons with a matching signature.
* =====================================================================================
*/
vector<Person> signatureSearch(std::string query, vector<Person> allPersons ) {
    vector<Person> personsFound;
    for (auto p : allPersons) {
        if ( p.signature == query) { personsFound.push_back(p); }
    }
    return personsFound;
}

/*
* ===  FUNCTION  ======================================================================
*         Name:  sortingPersons()
*  Description:  Sort the list with persons. By name, signature, height or random.
*        Input:  Takes sorting type as enum SortingChoice, and list with persons.
*       Output:  -
* =====================================================================================
*/
void sortingPersons(SortingChoice choice, vector<Person> &persons) {
    switch (choice)
    {
        case lastname:
            sort(persons.begin(), persons.end(), orderByLastName);
            break;
        case signature:
            sort(persons.begin(), persons.end(), orderBySignature);
            break;
        case height:
            sort(persons.begin(), persons.end(), orderByHeight);
            reverse(persons.begin(), persons.end());
            break;
        case randomize:
            shuffle(persons.begin(), persons.end(), default_random_engine(static_cast<unsigned long>(time(0))) );
            break;
        default:
            cout << "Ups... I didn't get that, sorry!" << endl;
            break;
    }

}

/*
* ===  FUNCTION  ======================================================================
*         Name:  showFindings()
*  Description:  Prints list with persons in a list.
*        Input:  Takes namelist (vector<std::string>)
*       Output:  -
* =====================================================================================
*/
void showFindings(std::vector<Person> personsFound) {
    if (personsFound.empty()) {
        cout << "Sorry, I didn't find any matching signatures in the list.";
    }
    else {
        if (personsFound.size() == 1) {
            cout << "I only found a single matching signature in the list." << endl;
            showList(personsFound);
            cout << endl;
        }
        else {
            cout << "I found these matching signatures in the list." << endl;
            showList(personsFound);
            cout << endl;
        }
    }
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  deletePerson()
 *  Description:  Delete a person from a list (vector<Person>).
 *        Input:  Person to be deleted (Person), and list to be deleted from (vector<Person>)
 *       Output:  -
 * =====================================================================================
 */
void deletePerson(string signature, std::vector<Person> &data) {
    // MAKE A VECTOR OF PERSONS MATCHING THE SEARCH KEY, AND HELP USER DELETE (EVEN IF MULTIPLE MATCHES)
    vector<Person> personsFound { signatureSearch(signature, data) };
    if (personsFound.empty()) {
        cout << "Sorry, I didn't find any matches for the signature '" + signature + "'." << endl;
    }
    else {
        showFindings(personsFound);

        // DELETING A PERSON FROM THE LIST
        int del_nr{1};
        if (personsFound.size() > 1) {
            del_nr = getInteger(
                    "Several matches found. Please select the sequential number (the first integer on the line)");
            if (!(1 <= del_nr && del_nr <= personsFound.size())) {
                cout << "Ups... I don't think that is a sequential number (I'll just assume you meant nr. 1).";
                del_nr = 1;
            }
        }
        int i{0};
        vector<Person>::iterator del_pos;
        for (del_pos = data.begin(); del_pos != data.end(); ++del_pos) {
            if ((*del_pos).signature == signature) {
                i++;
                if (i == del_nr) {
                    break;
                }
            }
        }
        cout << "Do you want to DELETE the following person from the list?" << endl;
        cout << "Signature: " << (*del_pos).signature << endl
             << "First name: " << (*del_pos).firstName << endl
             << "Last name: " << (*del_pos).lastName << endl
             << "Height: " << (*del_pos).height << endl;

        bool isYes{ yesOrNo("Please select yes [Y] or No [N] to delete above person from list.") };
        if (isYes) {
            string message{"Ok, I just deleted '"
                           + (*del_pos).firstName + " " + (*del_pos).lastName
                           + " ("
                           + (*del_pos).signature
                           + ")"
                           + "' from the list."};
            data.erase(del_pos);
            cout << message << endl;
        } else {
            cout << "Ok. Nothing is done.";
        }
    }

    // cleaning up stream. Others are happy when you clean up.
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  isUnique()
 *  Description:  Compare a person to a list of persons (based on name and height, case ignored).
 *        Input:  Person and a list of persons
 *       Output:  Bool true if person is unique. Meaning person not in the list already.
 * =====================================================================================
 */
bool isUnique(Person person, std::vector<Person> persons) {
    string name1 { person.firstName + person.lastName + to_string(person.height) };
    for (auto &c : name1) {
        c = static_cast<char> ( tolower(c) );
    }

    for (auto e : persons) {
        string name2 { e.firstName + e.lastName + to_string(e.height) };
        for (auto &c : name2) {
            c = static_cast<char> ( tolower(c) );
        }
        // IF LOWERCASE STRINGS WITH NAMES AND HEIGHT ARE EQUAL, THEN PERSON IS NOT UNIQUE.
        if (name1 == name2) {
            return false;
        }
    }
    return true;
}
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  addPersonOrRetry()
 *  Description:  Validate uniqueness of person when person is created from user-input.
 *                If person is not in list, then person is added to list (with push_back).
 *        Input:  Person and a list to search for uniqueness.
 *       Output:  Bool. True if BOTH person is not unique AND user choose to re-run input.
 *                Else return false (meaning if person i unique, then false is returned).
 * =====================================================================================
 */
bool addPersonOrRetry(Person person, std::vector<Person> &allPersons) {
    bool reTry {false};
    if ( isUnique(person, allPersons)) {
        allPersons.push_back(addSignature(person, allPersons));
    } else {
        cout << "I think that person is in the list already." << endl;
        reTry = yesOrNo("If you want to change input press [y]. If you want to cancel press [N].");
    }
    return reTry;
}
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  findDoublets()
 *  Description:  Find doublets based on name (case ignored) and height.
 *        Input:  Vector with persons.
 *       Output:  Vector with all doublets.
 *                Returns empty vector, if no doublets are found.
 * =====================================================================================
 */
vector<Person> findDoublets(std::vector<Person> persons) {
    vector<Person> doublets;
    if (persons.empty()) { return doublets; }

    // RUNS THROUGH THE LIST WITH TO ITERATOR, JUST LIKE TWO FINGERS MOVING THROUGH A LIST
    vector<Person>::iterator it1, it2;
    for (it1 = persons.begin(); it1 != (persons.end()-1); ++it1) {

        bool isFirst{true};   // bool to track if a person (it2) already has been detected as doublet.
        string name1{(*it1).firstName + (*it1).lastName + to_string((*it1).height)};

        for (it2 = (persons.begin() + 1); it2 != persons.end(); ++it2) {
            if (it1 == it2) { it2++; }
            string name2{(*it2).firstName + (*it2).lastName + to_string((*it2).height)};

            for (auto &c : name1) {
                c = static_cast<char> ( tolower(c) );
            }
            for (auto &c : name2) {
                c = static_cast<char> ( tolower(c) );
            }

            // COMPARE NAMES (AND HEIGHT) STRINGS AND PUSH TO DOUBLETS-LIST IF EQUAL
            if (name1 == name2) {
                if (it2 < it1) { break; }   // Because then the doublet has been found already.
                if (isFirst) {
                    doublets.push_back((*it1));
                    doublets.push_back((*it2));
                    isFirst = false;
                } else {
                    doublets.push_back((*it2));  // Because (*it1) is already in doublets.
                }
            }
        }
    }
    return doublets;
}

// = = = = = = = = = = = = = = = = = = = =
//      ORDER RELATION ON STRUCT PERSON
// = = = = = = = = = = = = = = = = = = = =

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  orderByLastName()
 *                orderBySignature()
 *                orderByHeight()
 *  Description:  Used as order relations on objects in the struct Person.
 *        Input:  -
 *       Output:  -
 * =====================================================================================
 */
bool orderByLastName(Person p1, Person p2) {
    std::string lhs { (p1.lastName + " " + p1.firstName) };
    std::string rhs { (p2.lastName + " " + p2.firstName) };

    for (auto &c : lhs) {
        c = tolower(c);
    }
    for (auto &c : rhs) {
        c = tolower(c);
    }
    return lhs < rhs;
}
bool orderBySignature(Person p1, Person p2) {
    return p1.signature < p2.signature;
}

bool orderByHeight(Person p1, Person p2) {
    return (p1.height < p2.height);
}

// = = = = = = = = = = = = = = = = = = = =
//      HANDLING USER INTERACTION
// = = = = = = = = = = = = = = = = = = = =
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  getString()
 *  Description:  Ask user for input via std.out.
 *        Input:  Questing/instruction to user (as a string)
 *       Output:  The string entered by the user.
 * =====================================================================================
 */
string getString(string question) {
    string str;
    while ( true ) {
        cout << question ;
        if ( getline(cin, str) ) {
            break;
        } else {
            cout << "Please enter a valid string" << endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
    }
    return str;
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  getInteger
 *  Description:  Ask user for integer input via std.out.
 *        Input:  Questing/instruction to user (as a string)
 *       Output:  The integer entered by the user.
 * =====================================================================================
 */
int getInteger(string question) {
    int sel;
    while ( true ) {
        cout << question ;
        if (cin >> sel) {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            break;
        } else {
            cout << "Please enter a valid integer" << endl;
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
        }
    }
    return sel;
}
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  waitForKey()
 *  Description:  Used to add a person to namelist, via input from user.
 *                Asks user for first name, last name (strings) and height (double)
 *                Validate ASCII and that DELIM is not used.
 *                Returns a Person-object
 *       Input:   -
 *      Output:   Person-object (a struct) as defined in constants.h
 * =====================================================================================
 */
Person inputPerson() {
    Person tempPerson;

    bool inputOK;
    do {
        inputOK = true;
        cout << "Please enter first name: ";
        getline(cin, tempPerson.firstName);
        for (auto &c : tempPerson.firstName) {
            if (c == DELIM) { c = DELIM_SUBSTITUTE; }
            if (c < 32 || c > 126) { inputOK = false; }            // ASCII in range [32; 126]
        }

        cout << "Please enter last name: ";
        getline(cin, tempPerson.lastName);
        for (auto &c : tempPerson.lastName) {
            if (c == DELIM) { c = DELIM_SUBSTITUTE; }
            if (c < 32 || c > 126) { inputOK = false; }            // ASCII in range [32; 126]
        }

        cout << "Please enter height: ";
        double h;
        cin >> h;
        tempPerson.height = round(h * 100) / 100;   // Just because assignment asks for (meters & float & 2 decimals)

        if (!inputOK) {
            cout << "Please only use ASCII characters (Sorry...)." << endl;
        }

        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    } while ( !inputOK );

    return tempPerson;
}
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  addSignature()
 *  Description:  assigns unique signature to a new person (form input).
 *        Input:  New Person, and the list to add the person to (needed for uniqueness).
 *       Output:  A person with a signature.
 * =====================================================================================
 */
Person addSignature(Person person, vector<Person> allPersons) {
    std::string fName{person.firstName};
    std::string lName{person.lastName};

    // REMOVE NON-ALPHA AND BLANK SPACE CHARACTERS IN NAMES
    for (auto &c : fName) if (!isalpha(c)) c = ' ';
    for (auto &c : lName) if (!isalpha(c)) c = ' ';

    std::string::iterator end_pos;
    end_pos = std::remove(fName.begin(), fName.end(), ' ');
    fName.erase(end_pos, fName.end());

    end_pos = std::remove(lName.begin(), lName.end(), ' ');
    lName.erase(end_pos, lName.end());

    // Fill in x's in short names
    constexpr unsigned SIGNATURE_SIZE{3};
    if (fName.size() < SIGNATURE_SIZE) {
        fName.append("xxx");
    }
    if (lName.size() < SIGNATURE_SIZE) {
        lName.append("xxx");
    }

    // Cat signature
    std::string signature;
    signature = fName.substr(0, 3);
    signature += lName.substr(0, 3);
    for (auto &c : signature) {
        c = static_cast<char> ( tolower(c));
    }

    // (Nearly) GUARANTEE SIGNATURE'S UNIQUENESS
    string s;
    string tempSig;
    for (int i = 0; i < 100; ++i) {
        tempSig = signature;
        if (i < 10) {
            s = "0" + to_string(i);
        } else {
            s = to_string(i);
        }
        tempSig.append(s);

        // SEARCH FOR SIGNATURE TO SEE IF IT EXISTS IN LIST ALREADY.
        vector<Person> tempVec = signatureSearch(tempSig,
                                                 allPersons);   // signatureSearch returns vector with all matches
        if (tempVec.empty()) {                // if empty then unique
            break;
        }
        if (i > 99) {
            cout
                    << "Sorry, I can't make this signature unique. Please look into it your self via find doublet in Main Menu."
                    << endl;
        }
    }
    person.signature = tempSig;
    return person;
}
//----------------------------------------
// Description:  Asks user to enter y(Y) or n(N) (yes or no)
//       Input:  question (string) - a text with a question
//      Output:  true (bool) - if user answer yes
//               false (bool) - if user answer no
//----------------------------------------
bool yesOrNo(string question)
{
    cout << question;
    char ch;
    do {
        cin >> ch;
        ch = toupper(ch);
    } while ( !(ch == 'Y' || ch == 'N') );

    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    return (ch == 'Y');
}

/*
 * ===  FUNCTION  ======================================================================
 *         Name:  waitForKey()
 *  Description:  Stops program until user presses enter.
 *        Input:  -
 *       Output:  -
 * =====================================================================================
 */
void waitForKey() {
    cout << "Press enter to continue!";
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
}

// = = = = = = = = = = = = = = = = = = = =
//      FILE HANDLING
// = = = = = = = = = = = = = = = = = = = =
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  saveToFile()
 *  Description:  Save a list to file. Ask user for filename.
 *        Input:  Vector with the list to save.
 *       Output:  -
 * =====================================================================================
 */

// Function saveToFile()
    void saveToFile(std::vector<Person> &allPersons) {
    if (allPersons.empty()) {
        cout << "There is nothing to save, the current list is empty." << endl;
    }
    else {
        cout << "Ok, lets save the list..." << endl;
        string fileName { getString("Please enter full path and filename (if file exists it will be overwritten): ") };
        ofstream outFile(fileName);
        string row;
        for (auto p : allPersons){
            row = (p.firstName + DELIM + p.lastName + DELIM + p.signature + DELIM + to_string(p.height) );

/*            // crypt ->
            for (auto &c : row) {
                // replace the letter in text with rotatet letter from contained in ascii sign [32,126] tvs. 95 characters.
                //c = static_cast<unsigned char> (c + rot);
                if ((c + rot) > 126) {
                    c = static_cast<char> ((c + rot) - 127 + 32 );
                } else {
                    c = static_cast<char> ( c + rot );
                }
            }
            // <- crypt*/

            outFile << row << endl;
        }
        outFile.close();
        cout << "The list is saved to the file " << fileName << endl;
    }

}
/*
 * ===  FUNCTION  ======================================================================
 *         Name:  loadFileTo()
 *  Description:  Load a list from file. Ask user for filename.
 *        Input:  Vector with the list to load persons into.
 *       Output:  -
 * =====================================================================================
 */
void loadFileTo(std::vector<Person> &allPersons) {
    bool ok{true};
    if (!allPersons.empty()) {
        ok = yesOrNo(
                "There are persons in current list. The current list will be lost when you load a file.\nDo you want to proceed (y/N)?: ");
    }
    if (ok) {
        allPersons.clear();
        string fileName { getString("Please enter full path and filename: ") };

        ifstream inFile(fileName);
        Person newPerson;
        while (getline(inFile, newPerson.firstName, DELIM)) {
            getline(inFile, newPerson.lastName, DELIM);
            getline(inFile, newPerson.signature, DELIM);
            inFile >> newPerson.height;
            inFile.get();
            allPersons.push_back(newPerson);
        }
        inFile.close();
        cout << "Ok, I have loaded the file." << endl;
    }
}

