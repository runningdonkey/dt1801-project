#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include "../include/constants.h"
#include "../include/funcLib.h"

using namespace std;

int main() {
    vector<Person> allPersons;

    bool exit {false};
    while ( !exit ) {
        vector<string> mainMenu { createEntries(MainMenu) } ;
        const int sel { showMenu( "**** MAIN MENU ****", mainMenu, allPersons.size()) };

        switch (sel) {
            case 1: { // ADD
                cout << "Ok, let us add a person to the list." << endl;
                bool reTry{false};
                do {
                    Person newPerson{ inputPerson() };
                    reTry = addPersonOrRetry(newPerson, allPersons);  // Validate uniqueness and push or ask to retry
                } while (reTry);
                break;
            }

            case 2: // SHOW LIST
                cout << "Ok, I will print the list with persons:" << endl;
                showList(allPersons);
                waitForKey();
                break;

            case 3: {  // SORTING
                vector<string> sortingMenu { createEntries(SortingMenu) };
                const int i  { showMenu("** SORTING MENU **", sortingMenu, allPersons.size()) };
                auto ch = static_cast<SortingChoice> ( i );

                sortingPersons(ch, allPersons);
                showList(allPersons);
                waitForKey();
                break;
            }

            case 4: { // SEARCH
                cout << "Ok, lets find some people..." << endl;
                string key = getString("Please enter the persons signature as search key: ");
                vector<Person> personsFound { signatureSearch(key, allPersons) };
                showFindings(personsFound);
                waitForKey();
                break;
            }

            case 5: { // DELETE
                cout << "Ok, lets DELETE some people..." << endl;
                string key = getString("Please enter the persons signature as search key: ");
                deletePerson(key, allPersons);
                waitForKey();
                break;
            }

            case 6: // SAVE TO FILE
                saveToFile(allPersons);
                waitForKey();
                break;

            case 7: { // LOAD FILE
                cout << "Ok, lets load some people into the list..." << endl;
                loadFileTo(allPersons);
                waitForKey();
                break;
            }

            case 8: {  // FIND DOUBLETS
                vector<Person> doublets { findDoublets(allPersons) };
                showList(doublets);
                waitForKey();
                break;
            }
            case 9: // EXIT
                cout << "Ok program now terminates. I hope you have enjoyed this program." << endl;
                exit = true;
                break;

            default:
                cout << "Ups... " << endl;
                cout << "That's not a correct choice. Please input a choice as an integer." << endl;
                waitForKey();
        }
    }
    return 0;
}
