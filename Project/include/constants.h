//
// Created by pauli on 1/13/19.
//

#ifndef DT018G_CONSTANTS_H
#define DT018G_CONSTANTS_H

#include <string>

struct Person
{
    std::string signature, firstName, lastName;
    double height;

    //std::string signature {firstName};

};

enum SortingChoice {lastname=1, signature, height, randomize };
enum MenuTitle {MainMenu, SortingMenu};

constexpr char DELIM {'|'};
constexpr char DELIM_SUBSTITUTE {' '};
constexpr char INVALID_SUBSTITUTE {'X'};

#endif //DT018G_CONSTANTS_H
