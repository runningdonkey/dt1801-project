//
// Created by pauli on 1/13/19.
//

#ifndef DT018G_FUNCLIB_H
#define DT018G_FUNCLIB_H

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <locale>
#include <random>
#include <sstream>
#include <string>
#include <vector>

#include "constants.h"

using std::string;
using std::vector;



// MENUES AND PRINTINGS
vector<string> createEntries(MenuTitle menu);
int showMenu(string menuTitle, vector<string> entries, unsigned long total );
void showList(vector<Person> data);

// SEARCHING AND DELETING
vector<Person> signatureSearch(string query, vector<Person> allPersons);
void sortingPersons(SortingChoice choice, vector<Person> &persons);
void showFindings(vector<Person> personsFound);
void deletePerson(string signature, vector<Person> &data);
bool isUnique(Person person, vector<Person> persons);
bool addPersonOrRetry(Person person, vector<Person> &allPersons);
vector<Person> findDoublets(vector<Person> persons);


// ORDER RELATION ON STRUCT PERSON
bool orderByLastName(Person p1, Person p2);
bool orderBySignature(Person p1, Person p2);
bool orderByHeight(Person p1, Person p2);

// HANDLING USER INTERACTION
string getString(string question);
int getInteger(string question);
Person inputPerson();
Person addSignature(Person person, vector<Person> persons);
bool yesOrNo(string question);
void waitForKey();

// FILE HANDLING
void saveToFile(vector<Person> &allPersons);
void loadFileTo(vector<Person> &allPersons);

#endif //DT018G_FUNCLIB_H
